import express from 'express';
import {
	allGames,
	gameById,
	gameBySteamAppid,
	searchGames,
	searchKeyWordGames
} from '../controllers/game.controller.js';

const router = new express.Router();

router.get('/', allGames);
router.get('/search', searchGames);
router.get('/searchOne', searchGames);
router.get('/:id', gameById);
router.get('/steam-appid/:steamAppid', gameBySteamAppid);
router.post('/searchkey', searchKeyWordGames);

export default router;
