import express from 'express';
import { allGenres } from '../controllers/genre.controller.js';

const router = new express.Router();

router.get('/', allGenres);

export default router;
