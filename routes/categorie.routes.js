import express from 'express';
import { allCategories } from '../controllers/categorie.controller.js';

const router = new express.Router();

router.get('/', allCategories);

export default router;
