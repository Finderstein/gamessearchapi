import Game from '../models/game.model.js';
import { StatusCodes } from 'http-status-codes';

export async function allGenres(req, res) {
	try {
		let gamesGenres = await Game.getAllGenres();

		const genresSet = new Set();

		gamesGenres.forEach((game) =>
			game.genres.forEach((genre) => genresSet.add(genre))
		);

		res.status(StatusCodes.OK).json({ genres: [...genresSet] });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}
