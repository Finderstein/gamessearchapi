import Game from '../models/game.model.js';
import { StatusCodes } from 'http-status-codes';

export async function allCategories(req, res) {
	try {
		let gamesCategories = await Game.getAllCategories();

		const categoriesSet = new Set();

		gamesCategories.forEach((game) =>
			game.categories.forEach((categorie) => categoriesSet.add(categorie))
		);

		res.status(StatusCodes.OK).json({ categories: [...categoriesSet] });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}
