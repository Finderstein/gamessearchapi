import Game from '../models/game.model.js';
import { StatusCodes } from 'http-status-codes';
import fetch from 'node-fetch';
import GameWithKeywords from '../models/game.with.keywords.model.js';
import {toString} from 'nlcst-to-string';
import {retext} from 'retext';
import retextPos from 'retext-pos';
import retextKeywords from 'retext-keywords';

const DEFAULT_LIMIT = 10;

const re = retext()
  .use(retextPos) // Make sure to use `retext-pos` before `retext-keywords`.
  .use(retextKeywords);

export async function allGames(req, res) {
	let { offset, limit } = req.query;
	offset = offset ? +offset : 0;
	limit = limit ? +limit : DEFAULT_LIMIT;

	try {
		let games = await Game.getWithOffsetLimit(offset, limit);

		let updGames = await Promise.all(
			games.map(async (game) => {
				const response = await fetch(
					'https://store.steampowered.com/api/appdetails?appids=' +
						game.steam_appid
				);
				const data = await response.json();
				const imageSrc = data[game.steam_appid].data.header_image;
				return { ...game, imageSrc };
			})
		);

		res
			.status(StatusCodes.OK)
			.json({ offset, limit, count: updGames.length, games: updGames });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function gameById(req, res) {
	const { id } = req.params;

	if (!id) {
		res.status(StatusCodes.OK).json({ message: 'No id' });
	}

	try {
		let game = await Game.getById(id);

		if (!game) {
			res.status(StatusCodes.OK).json({ message: 'No game with this id' });
		}

		const response = await fetch(
			'https://store.steampowered.com/api/appdetails?appids=' + game.steam_appid
		);
		const data = await response.json();
		const imageSrc = data[game.steam_appid].data.header_image;
		const updGame = { ...game, imageSrc };

		res.status(StatusCodes.OK).json({ game: updGame });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function gameBySteamAppid(req, res) {
	const { steamAppid } = req.params;

	if (!steamAppid) {
		res.status(StatusCodes.OK).json({ message: 'No Steam App Id' });
	}

	try {
		let game = await Game.getBySteamAppid(steamAppid);

		if (!game) {
			res
				.status(StatusCodes.OK)
				.json({ message: 'No game with this Steam App Id' });
		}

		const response = await fetch(
			'https://store.steampowered.com/api/appdetails?appids=' + game.steam_appid
		);
		const data = await response.json();
		const imageSrc = data[game.steam_appid].data.header_image;
		const updGame = { ...game, imageSrc };

		res.status(StatusCodes.OK).json({ game: updGame });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function searchGames(req, res) {
	let { name, genres, categories, offset, limit } = req.body;
	const path = req.path;
	offset = offset ? +offset : 0;
	limit = limit ? +limit : DEFAULT_LIMIT;

	try {
		let games = await Game.searchGames(name, genres, categories, offset, limit);

		let updGames = await Promise.all(
			games.map(async (game) => {
				const response = await fetch(
					'https://store.steampowered.com/api/appdetails?appids=' +
						game.steam_appid
				);
				const data = await response.json();
				const imageSrc = data[game.steam_appid].data.header_image;
				return { ...game, imageSrc };
			})
		);

		if (path === '/searchOne') {
			const game = updGames[0];
			const additionalMessage =
				' (Keep in mind that the name of the game is compared strictly)';
			if (game.name === name) {
				return res.status(StatusCodes.OK).json({
					name,
					genres,
					categories,
					message:
						'Success! Game with this parameters is found!' + additionalMessage,
					game,
					otherPossibleSearchedGames: updGames.slice(1)
				});
			}

			return res.status(StatusCodes.OK).json({
				name,
				genres,
				categories,
				message:
					'Fail! No game with this parameters is found!' + additionalMessage,
				possibleSearchedGames: updGames.slice(1)
			});
		}

		res.status(StatusCodes.OK).json({
			name,
			genres,
			categories,
			offset,
			limit,
			count: updGames.length,
			games: updGames
		});
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function searchKeyWordGames(req, res) {
	let { query, offset, limit } = req.body;
	offset = offset ? +offset : 0;
	limit = limit ? +limit : DEFAULT_LIMIT;

	try {
		const proccessed = await re.process(query.toLowerCase());

		const keywords = proccessed.data.keywords
			.map(keyword => toString(keyword.matches[0].node));
        const keyphrases = proccessed.data.keyphrases
			.map(phrase => phrase.matches[0].nodes.map((d) => toString(d)).join(''));

		let games = await GameWithKeywords.searchGames(keywords, keyphrases, offset, limit);

		let updGames = await Promise.all(
			games.map(async (game) => {
				const response = await fetch(
					'https://store.steampowered.com/api/appdetails?appids=' +
						game.steam_appid
				);
				const data = await response.json();
				const imageSrc = data[game.steam_appid].data.header_image;
				return { ...game, imageSrc };
			})
		);

		res.status(StatusCodes.OK).json({
			query,
			offset,
			limit,
			count: updGames.length,
			games: updGames
		});
	} catch (err) {
		console.log(err);

		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error', kk: err });
	}
}
