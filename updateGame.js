/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
// eslint-disable-next-line no-unused-vars
import {toString} from 'nlcst-to-string'
import {retext} from 'retext'
import retextPos from 'retext-pos'
import retextKeywords from 'retext-keywords'
import Game from './models/game.model.js'
import mongoose from 'mongoose'
import GameWithKeywords from './models/game.with.keywords.model.js'

const file = 'Ultimate EditionThe Red Dead Redemption 2:  Ultimate Edition delivers all of the Story Mode content from the Special Edition plus additional content for online including Bonus Outfits for your online Character, Rank Bonuses, Black Chestnut Thoroughbred and free access to the Survivor Camp Theme.Plus get free access to Additional Weapons in online.About the GameAmerica, 1899.Arthur Morgan and the Van der Linde gang are outlaws on the run. With federal agents and the best bounty hunters in the nation massing on their heels, the gang must rob, steal and fight their way across the rugged heartland of America in order to survive. As deepening internal divisions threaten to tear the gang apart, Arthur must make a choice between his own ideals and loyalty to the gang who raised him.Now featuring additional Story Mode content and a fully-featured Photo Mode, Red Dead Redemption 2 also includes free access to the shared living world of Red Dead Online, where players take on an array of roles to carve their own unique path on the frontier as they track wanted criminals as a Bounty Hunter, create a business as a Trader, unearth exotic treasures as a Collector or run an underground distillery as a Moonshiner and much more.With all new graphical and technical enhancements for deeper immersion, Red Dead Redemption 2 for PC takes full advantage of the power of the PC to bring every corner of this massive, rich and detailed world to life including increased draw distances; higher quality global illumination and ambient occlusion for improved day and night lighting; improved reflections and deeper, higher resolution shadows at all distances; tessellated tree textures and improved grass and fur textures for added realism in every plant and animal.Red Dead Redemption 2 for PC also offers HDR support, the ability to run high-end display setups with 4K resolution and beyond, multi-monitor configurations, widescreen configurations, faster frame rates and more.America, 1899.Arthur Morgan and the Van der Linde gang are outlaws on the run. With federal agents and the best bounty hunters in the nation massing on their heels, the gang must rob, steal and fight their way across the rugged heartland of America in order to survive. As deepening internal divisions threaten to tear the gang apart, Arthur must make a choice between his own ideals and loyalty to the gang who raised him.Now featuring additional Story Mode content and a fully-featured Photo Mode, Red Dead Redemption 2 also includes free access to the shared living world of Red Dead Online, where players take on an array of roles to carve their own unique path on the frontier as they track wanted criminals as a Bounty Hunter, create a business as a Trader, unearth exotic treasures as a Collector or run an underground distillery as a Moonshiner and much more.With all new graphical and technical enhancements for deeper immersion, Red Dead Redemption 2 for PC takes full advantage of the power of the PC to bring every corner of this massive, rich and detailed world to life including increased draw distances; higher quality global illumination and ambient occlusion for improved day and night lighting; improved reflections and deeper, higher resolution shadows at all distances; tessellated tree textures and improved grass and fur textures for added realism in every plant and animal.Red Dead Redemption 2 for PC also offers HDR support, the ability to run high-end display setups with 4K resolution and beyond, multi-monitor configurations, widescreen configurations, faster frame rates and more.';

const re = retext()
  .use(retextPos) // Make sure to use `retext-pos` before `retext-keywords`.
  .use(retextKeywords);

// re
//   .process(file)
//   .then((file) => {
//     console.log('Keywords:')
//     file.data.keywords.forEach((keyword) => {
//       console.log(toString(keyword.matches[0].node))
//     })

//     console.log()
//     console.log('Key-phrases:')
//     file.data.keyphrases.forEach((phrase) => {
//       console.log(phrase.matches[0].nodes.map((d) => toString(d)).join(''))
//     })
//   })

  

const databaseUrl = 'mongodb+srv://new_user:fOp2iUensFDqWfIt@cluster0.raewz.mongodb.net/Games?retryWrites=true&w=majority';

mongoose
	.connect(databaseUrl, { useNewUrlParser: true })
	.then(() => console.log(`Database connected!`))
    .then(async () => await Game.count())
    .then(async (num) => {
      await re
      .process(file)
      .then(async (file) => {
        console.log('Keywords:')
        file.data.keywords.forEach((keyword) => {
          console.log(toString(keyword.matches[0].node))
        })
    
        console.log()
        console.log('Key-phrases:')
        file.data.keyphrases.forEach((phrase) => {
          console.log(phrase.matches[0].nodes.map((d) => toString(d)).join(''))
        })
    
        const keywords = file.data.keywords.map(keyword => toString(keyword.matches[0].node));
        const keyphrases = file.data.keyphrases.map(phrase => phrase.matches[0].nodes.map((d) => toString(d)).join(''));
    
        // eslint-disable-next-line no-magic-numbers
        let games = await GameWithKeywords.searchGames(keywords, keyphrases, 0, 20);
    
        console.log(games);
      }) 
    });

// mongoose
// 	.connect(databaseUrl, { useNewUrlParser: true })
// 	.then(() => console.log(`Database connected!`))
//     .then(async () => await Game.count())
//     .then(async (num) => {
//         console.log(num);

//         for (let i = 0; i<num; i++) {
//           let g = await Game.getWithOffsetLimit(i, 1);

//           const gk = await re
//             .process(g[0].description)
//             .then((file) => {
//                 g[0].keywords = file.data.keywords.map(keyword => toString(keyword.matches[0].node));
//                 g[0].keyphrases = file.data.keyphrases.map(phrase => phrase.matches[0].nodes.map((d) => toString(d)).join(''));
//                 return g[0];
//             }, (reason) => { 
//               console.log(reason); 
//               return g[0]; 
//             });
//           await GameWithKeywords.create(gk);

//           // eslint-disable-next-line no-magic-numbers
//           console.log((i + 1) / num * 100);
//         }  
//     });
  