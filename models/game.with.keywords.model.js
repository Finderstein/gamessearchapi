import mongoose from 'mongoose';

const GameSchema = new mongoose.Schema({
	steam_appid: { type: Number, required: true },
	name: { type: String, required: true },
	description: { type: String },
	genres: [{ type: String }],
	categories: [{ type: String }],
    keywords: [{type: String}],
    keyphrases: [{type: String}]
});

const GameModelK = mongoose.model('gamewithkeywords', GameSchema);

class GameWithKeywords {
	static getById(id) {
		return GameModelK.findById({ _id: id }).lean();
	}
	static getBySteamAppid(steam_appid) {
		return GameModelK.findOne({ steam_appid: steam_appid }).lean();
	}

	static getWithOffsetLimit(offset, limit) {
		return GameModelK.find().skip(offset).limit(limit).lean();
	}

    static create(model){
        return GameModelK.create(model);
    }

	static getAllGenres() {
		return GameModelK.find().select('genres -_id').lean();
	}

	static getAllCategories() {
		return GameModelK.find().select('categories -_id').lean();
	}

	static count() {
		return GameModelK.count({}, function(error, numOfDocs) {
			console.log('I have '+numOfDocs+' documents in my collection');
			console.log(error)
			// ..
		});
	}

	static searchGames(keywords, keyphrases, offset, limit) {
		const searchQuery = {};
		if (keywords && keywords.length !== 0) {
			searchQuery.keywords = {
				$in: keywords 
			}
		}
		if (keyphrases && keyphrases.length !== 0) {
			searchQuery.keyphrases = { $in: keyphrases };
		}
		
		return GameModelK.find(searchQuery).skip(offset).limit(limit).lean();
	}
}

export default GameWithKeywords;
