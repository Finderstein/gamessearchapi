import mongoose from 'mongoose';

const GameSchema = new mongoose.Schema({
	steam_appid: { type: Number, required: true },
	name: { type: String, required: true },
	description: { type: String },
	genres: [{ type: String }],
	categories: [{ type: String }]
});

const GameModel = mongoose.model('Game', GameSchema);

class Game {
	static getById(id) {
		return GameModel.findById({ _id: id }).lean();
	}
	static getBySteamAppid(steam_appid) {
		return GameModel.findOne({ steam_appid: steam_appid }).lean();
	}

	static getWithOffsetLimit(offset, limit) {
		return GameModel.find().skip(offset).limit(limit).lean();
	}

	static getAllGenres() {
		return GameModel.find().select('genres -_id').lean();
	}

	static getAllCategories() {
		return GameModel.find().select('categories -_id').lean();
	}

	static count() {
		return GameModel.count();
	}

	static searchGames(name, genres, categories, offset, limit) {
		const searchQuery = {};
		if (name) {
			searchQuery.name = { $regex: name, $options: 'i' };
		}
		if (genres) {
			searchQuery.genres = { $all: genres };
		}
		if (categories) {
			searchQuery.categories = { $all: categories };
		}

		return GameModel.find(searchQuery).skip(offset).limit(limit).lean();
	}
}

export default Game;
